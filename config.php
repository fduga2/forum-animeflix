<?php return array (
  'debug' => false,
  'database' => 
  array (
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'forumAnimeflix',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => 'flarum_',
    'port' => '3306',
    'strict' => false,
  ),
  'url' => 'http://localhost/forumAnimeflix/public',
  'paths' => 
  array (
    'api' => 'api',
    'admin' => 'admin',
  ),
);