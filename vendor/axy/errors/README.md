# axy\errors: exceptions in PHP
 
 * GitHub: [axypro/errors](https://github.com/axypro/errors)
 * Composer: [axy/errors](https://packagist.org/packages/axy/errors)

[Documentation](https://github.com/axypro/errors/blob/master/doc/README.md).

PHP 5.4+

The library does not require any dependencies (except composer packages).
